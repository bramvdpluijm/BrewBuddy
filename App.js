import React from 'react';
import { createStackNavigator } from 'react-navigation';
import { Font } from 'expo';

import Recipes from 'screens/Recipes';
import EditRecipe from 'screens/EditRecipe';
import CreateRecipe from 'screens/CreateRecipe';

import Loader from 'components/Loader';

const RootStackNavigator = createStackNavigator({
  Recipes: {
    screen: Recipes,
  },
  EditRecipe: {
    screen: EditRecipe,
  },
  CreateRecipe: {
    screen: CreateRecipe,
  }
}, {
  cardStyle: { backgroundColor: '#F5F5F8' },
});

class App extends React.Component {
  state = {
    fontLoaded: false,
  }

  async componentDidMount() {
    await Font.loadAsync({
      'futura': require('./assets/fonts/Futura.ttf'),
    });

    this.setState({ fontLoaded: true });
  }

  render() {
      return (
        this.state.fontLoaded ? (
          <RootStackNavigator />
        ) : (
          <Loader />
        )
      )
  }
}

export default App;
