import React from 'react';
import { RecipeContainer } from './styled';

const RecipeItem = ({ children }) => (
  <RecipeContainer>
    {children}
  </RecipeContainer>
)

export default RecipeItem;
