import styled from 'styled-components/native';

export const RecipeContainer = styled.View`
  padding: 15px 20px;
  border-radius: 5px;
  background-color: #fff;
  shadow-opacity: 1;
  shadow-radius: 10px;
  shadow-color: #DADAE0;
  shadow-offset: 0px 0px;
  elevation: 10px;
  width: 100%;
  margin-bottom: 20px;
`;
