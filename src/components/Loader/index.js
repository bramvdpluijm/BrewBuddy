import React from 'react';
import { View, Text } from 'react-native';

// TODO: Make this thing a bit more shiny
class Loader extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}><Text>App is loading data</Text></View>
    )
  }
}

export default Loader;
