import React from 'react';
import { ScrollView, Text, Button, StatusBar, Image } from 'react-native';

class EditRecipe extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title'),
    };
  };

  render() {
    return (
      <ScrollView contentContainerStyle={{ flex: 1, justifyContent: 'center', paddingHorizontal: 20}}>
        <StatusBar
          barStyle="dark-content"
          backgroundColor="#FE602E"
        />
      </ScrollView>
    )
  }
}

export default EditRecipe;
