import React, { Fragment } from 'react';
import { ScrollView, Text, Button, StatusBar, Image, TouchableOpacity, TouchableWithoutFeedback } from 'react-native';
import RecipeItem from 'components/RecipeItem';
import { BeerName, BeerDetails, BeerType, BeerType as BeerPercentage, CreateButton } from './styled';

class Recipes extends React.Component {
  static navigationOptions = {
    title: 'Recepten',
  };

  render() {
    return (
      <Fragment>
        <ScrollView contentContainerStyle={{ paddingHorizontal: 20, paddingTop: 20, paddingBottom: 170 }}>
          <StatusBar
            barStyle="dark-content"
            backgroundColor="#FE602E"
          />

          <TouchableOpacity onPress={() => this.props.navigation.navigate('EditRecipe', { title: "Gouden Tripel" })}>
            <RecipeItem>
              <BeerName>Gouden tripel</BeerName>
              <Image source={require('assets/wiggle.png')} />
              <BeerDetails>
                <BeerType>Belgian Tripel</BeerType>
                <BeerPercentage>9.1%</BeerPercentage>
              </BeerDetails>
            </RecipeItem>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.props.navigation.navigate('EditRecipe', { title: "Honey Badger Wheatbeer" })}>
            <RecipeItem>
              <BeerName>Honey Badger Wheatbeer</BeerName>
              <Image source={require('assets/wiggle.png')} />
              <BeerDetails>
                <BeerType>American Wheat Beer</BeerType>
                <BeerPercentage>4.2%</BeerPercentage>
              </BeerDetails>
            </RecipeItem>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.props.navigation.navigate('EditRecipe', { title: "Honey Badger Wheatbeer" })}>
            <RecipeItem>
              <BeerName>Honey Badger Wheatbeer</BeerName>
              <Image source={require('assets/wiggle.png')} />
              <BeerDetails>
                <BeerType>American Wheat Beer</BeerType>
                <BeerPercentage>4.2%</BeerPercentage>
              </BeerDetails>
            </RecipeItem>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.props.navigation.navigate('EditRecipe', { title: "Honey Badger Wheatbeer" })}>
            <RecipeItem>
              <BeerName>Honey Badger Wheatbeer</BeerName>
              <Image source={require('assets/wiggle.png')} />
              <BeerDetails>
                <BeerType>American Wheat Beer</BeerType>
                <BeerPercentage>4.2%</BeerPercentage>
              </BeerDetails>
            </RecipeItem>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.props.navigation.navigate('EditRecipe', { title: "Honey Badger Wheatbeer" })}>
            <RecipeItem>
              <BeerName>Honey Badger Wheatbeer</BeerName>
              <Image source={require('assets/wiggle.png')} />
              <BeerDetails>
                <BeerType>American Wheat Beer</BeerType>
                <BeerPercentage>4.2%</BeerPercentage>
              </BeerDetails>
            </RecipeItem>
          </TouchableOpacity>
        </ScrollView>

        <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('CreateRecipe')}>
          <CreateButton>
            <Image source={require('assets/plus.png')} />
          </CreateButton>
        </TouchableWithoutFeedback>
      </Fragment>
    );
  }
}

export default Recipes;
