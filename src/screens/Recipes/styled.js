import styled from 'styled-components/native';

export const BeerName = styled.Text`
  font-family: 'futura';
  font-size: 25px;
  letter-spacing: 2.5px;
  margin-bottom: 15px;
`;

export const BeerDetails = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-top: 15px;
`;

export const BeerType = styled.Text`
  font-family: 'futura';
  font-size: 15px;
  letter-spacing: 1.5px;
`;

export const CreateButton = styled.View`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 85px;
  width: 85px;
  border-radius: 42.5;
  position: absolute;
  bottom: 50px;
  right: 20px;
  background-color: #fff;
  shadow-opacity: 1;
  shadow-radius: 10px;
  shadow-color: #DADAE0;
  shadow-offset: 0px 0px;
  elevation: 10px;
`;
