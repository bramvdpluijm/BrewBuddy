# BrewBuddy
A buddy that helps you brew the perfect craft beer! This app is currently in development phase.


## Feature roadmap
- **Brew notes:** Save some notes for the next time you brew the same recipe.
- **Brew timer:** This will make sure the right ingredients will be added to your brew at the right time. The app will send the user a push notification when ingredients have to be added.
- **Unit conversion:** From gallons to liters, from kilograms to pounds depending on the settings you provide in the settings screen.
- **Alcohol by Volume calculation:** Use this to document the alcohol amount of your recipes.
- **Ingredient calculator per batch amount:** Fill in your ingredients for a 20 liter brew, let the app calculate how much ingredients you will need if you decide to brew 60 liters.
- **Stock management:** Keep an eye on the grains you have in stock. When you buy new fermentables, hops or yeast, fill the amount into the app and it will handle the rest.
